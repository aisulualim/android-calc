package stepDefs;

import hooks.Hooks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import pageObjects.CalculatorPageObject;

import java.text.DecimalFormat;

public class CalculationStepDefs {
    private WebDriver driver = Hooks.driver;
    private CalculatorPageObject calculatorPageObject = new CalculatorPageObject(driver);
    private String actualResult = "";

    @Given("User opens the calculator app")
    public void userOpensTheCalculatorApp() {

    }

    @When("user adds {string}")
    public void userAdds(String num) {
        separateDigit(num);
        calculatorPageObject.clickEqualSign();
    }

    public void separateDigit(String number) {
        char[] symbols = number.toCharArray();
        for (int x = 0; x < symbols.length; x++) {
            if (symbols[x] == 0)
                calculatorPageObject.clickButton0();
            else if (symbols[x] == '1')
                calculatorPageObject.clickButton1();
            else if (symbols[x] == '2')
                calculatorPageObject.clickButton2();
            else if (symbols[x] == '3')
                calculatorPageObject.clickButton3();
            else if (symbols[x] == '4')
                calculatorPageObject.clickButton4();
            else if (symbols[x] == '5')
                calculatorPageObject.clickButton5();
            else if (symbols[x] == '6')
                calculatorPageObject.clickButton6();
            else if (symbols[x] == '7')
                calculatorPageObject.clickButton7();
            else if (symbols[x] == '8')
                calculatorPageObject.clickButton8();
            else if (symbols[x] == '9')
                calculatorPageObject.clickButton9();
            else if (symbols[x] == '+')
                calculatorPageObject.clickAddSign();
            else if (symbols[x] == '-')
                calculatorPageObject.clickSubtract();

        }
    }

    @When("takes the square root of that sum")
    public void takesTheSquareRootOfThatSum() {
        String result = calculatorPageObject.getResult();
        calculatorPageObject.clickClear();
        calculatorPageObject.clickSqrtSign();
        separateDigit(result);
        calculatorPageObject.clickEqualSign();
    }

    @Then("multiply it by {string}")
    public void multiplyItBy(String num) {
        calculatorPageObject.clickMulSign();
        separateDigit(num);
        calculatorPageObject.clickEqualSign();
    }

    @Then("assert that the answer is correctly displayed as {string}")
    public void assertThatTheAnswerIsCorrectlyDisplayedAs(String num) {
        actualResult = calculatorPageObject.getResult().replaceAll("−","-");
        calculatorPageObject.clickClear();
        System.out.println(actualResult);
        Assert.assertEquals(actualResult, num, "Validating full decimal Result");

    }

    // I think the value of the decimal expression is too large to parse into a java object like double
    // lets try cutting off the string and then formatting it

    @Then("assert that the value rounded up to the 4th decimal place is {string}")
    public void assertThatTheValueRoundedUpToThe4thDecimalPlaceIs(String num) {
        DecimalFormat decimalFormat = new DecimalFormat(".####");
        double result = Double.valueOf(actualResult);
        System.out.println("============");
        String res = decimalFormat.format(result);
        Assert.assertEquals(res, num, "Validating number value after rounding up to 4 decimal places");
    }
}
