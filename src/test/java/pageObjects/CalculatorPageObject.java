package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CalculatorPageObject {

    public CalculatorPageObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_0']")
    private WebElement button0;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_1']")
    private WebElement button1;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_2']")
    private WebElement button2;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_3']")
    private WebElement button3;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_4']")
    private WebElement button4;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_5']")
    private WebElement button5;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_6']")
    private WebElement button6;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_7']")
    private WebElement button7;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_8']")
    private WebElement button8;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/digit_9']")
    private WebElement button9;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/eq']")
    private WebElement equalSign;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/op_sqrt']")
    private WebElement sqrtSign;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/op_mul']")
    private WebElement mulSign;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/op_add']")
    private WebElement addSign;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/result']")
    private WebElement result;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/clr']")
    private WebElement clear;

    @FindBy(xpath = "//*[@resource-id='com.android.calculator2:id/op_sub']")
    private WebElement subtract;

    public void clickSubtract() {
        subtract.click();
    }

    public void clickClear() {
        clear.click();
    }

    public void clickButton0() {
        button0.click();
    }

    public void clickButton1() {
        button1.click();
    }

    public void clickButton2() {
        button2.click();
    }

    public void clickButton3() {
        button3.click();
    }

    public void clickButton4() {
        button4.click();
    }

    public void clickButton5() {
        button5.click();
    }

    public void clickButton6() {
        button6.click();
    }

    public void clickButton7() {
        button7.click();
    }

    public void clickButton8() {
        button8.click();
    }

    public void clickButton9() {
        button9.click();
    }

    public void clickMulSign() {
        mulSign.click();
    }

    public void clickAddSign() {
        addSign.click();
    }

    public void clickSqrtSign() {
        sqrtSign.click();
    }

    public void clickEqualSign() {
        equalSign.click();
    }

    public String getResult() {
        return result.getText();
    }
}
