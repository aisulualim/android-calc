Feature: Testing Calculator

  @calculation
  Scenario: As a Customer when I perform a calculation, I want the answer displayed completely after pressing
  the = button and evaluated by rounding up  to the 4th decimal place.
    Given User opens the calculator app
    When user adds "23+95"
    And takes the square root of that sum
    And multiply it by "-1"
    Then assert that the answer is correctly displayed as "-10.862782780491200215723891499337473"
    Then assert that the value rounded up to the 4th decimal place is "-10.8628"

  @calculation
  Scenario: As a Customer when I perform a calculation, I want the answer displayed completely after pressing
  the = button and evaluated by rounding up  to the 4th decimal place.
    Given User opens the calculator app
    When user adds "23+95"
    And takes the square root of that sum
    And multiply it by "-1"
    Then assert that the answer is correctly displayed as "-10.86278049120021572389149337473"
    Then assert that the value rounded up to the 4th decimal place is "-10.8628"