# Mobile testing requirements
Requirements:

1. JDK 8 or less. Android does not support java versions over version 8.

2. Maven for build management

3. IntelliJ or Eclipse

4. Android Studio for working with Android Virtual Devices (latest version)

5. Appium (latest)

Setup:

1. Install JDK 8 and add JAVA_HOME variable.
 
    ``` export JAVA_HOME=/Library/Java/JavaVirtualMachines/adoptopenjdk-8-openj9.jdk/Contents/Home ```
    
2. Install Android Studio and add ANDROID_HOME variable.

    ``` export ANDROID_HOME=/Users/aisulurav/Library/Android/sdk ```
    
3. Add other folders in ANDROID_HOME location to path.

    ``` export PATH=$PATH:$ANDROID_HOME/platform-tools ```

    ``` export PATH=$PATH:$ANDROID_HOME/tools ```

    ``` export PATH=$PATH:$ANDROID_HOME/tools/bin ```

    ``` export PATH=$PATH:$ANDROID_HOME/emulator ```

4. Run uiautomatorviewer from terminal to confirm that setup is working as expected.

5. Install appium and start up the service on 127.0.0.1:4723

6. Start Android Virtual Device in Android Studio. For this example, we have used a Galaxy Nexus device running Android Oreo 8.1.0.

7. Configure appium to connect to the phone using settings.


Running the tests:

- From IntelliJ: Open the project in IntelliJ, navigate to TestRunner class and run the TestRunner class to execute tests.

- From commandline: 

    - ```mvn clean test -Dcucumber.filter.tags="@calculation" -U -X``` (can be used on a CI/CD pipeline to execute tests)